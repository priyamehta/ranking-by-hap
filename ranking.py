#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 09:58:45 2019

@author: user

Ranking
"""

def read_file(file_name):
    return [ line.strip() for line in open(file_name, "r") ]

def make_dic(file_name):
    lines = read_file(file_name)
    dic = {}
    
    for line in lines:
        words = [word.strip() for word in line.split()]
        dic[words[0]] = int(words[1])
        
    return dic

def sort_dic(fileName):
    import operator
    x = make_dic(fileName)
    sorted_x = sorted(x.items(), key=operator.itemgetter(1), reverse=True)
    return sorted_x



print(sort_dic("marklist.txt"))